<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'wp_enter' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';A4.>zcMBZ&4ny{H)z-)FR g.%e~n%^$Ny4i~)?1 v!d5t1GfuxRS-)N,vR5[-Ho' );
define( 'SECURE_AUTH_KEY',  '!1>>MGxM?0 %9#W>VS~tt>_o?MKfim[2j3ohM0ziXjuNAE,mhf&qv7x5Iw(mxuQn' );
define( 'LOGGED_IN_KEY',    '[_[uz.pl+TC,%|`y[Rt):g}L;>%Rm]7*=jTB)qyn)m=5n!?TPrJ!Lm?_NP&PC{Lv' );
define( 'NONCE_KEY',        '-czHy6A2:A$+jyKt~%A+hsVlNaxo+mh,^WJS@c(wanNa@x{fYik|)9$#B#=iXzkg' );
define( 'AUTH_SALT',        'f{ewT &Ql:.:`3yZ|QfhqdEMSWL0P;a)j.k;@4(kt|#k$%m@Ve+-]wAYa|7Is6fp' );
define( 'SECURE_AUTH_SALT', 'D!-&aUCS3$Y*^`U^YO$j 8Qt*Tn#+AD*epLj^0w@MpHy`ajt{Pr>B`fe7/=69yNV' );
define( 'LOGGED_IN_SALT',   'UhE6s5Srwt?G.{;^c~,E_j(R4CKjeoa^;nSX?t=[c;:p1ElOYRtlrtxnYq-CtDyw' );
define( 'NONCE_SALT',       '=I4f?Z:)r,!390K[Ok2b)!By7iRNQ{Ve0y?vErMsgw{y+E7U|G1b6fe26=nBQT{k' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix  = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
