<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
        <section id="contact" class="py-8">
        <div class="container">
            <div class="columns mb-7">
                <div class="column"></div>
                <div class="column">
                    <div class="title">
                        <h2 class="has-text-weight-bold">Contact</h2>
                    </div>
                    <div class="level">
                        <div class="level-left">
                            <span class="slash is-italic">/</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-offset-6">
                    <p class="is-size-5 mb-4"><?php echo theme_option('address'); ?></p>
                    <p class="is-size-5 mb-4"><?php echo theme_option('sdt'); ?></p>
                    <p class="is-size-5 mb-2"><?php echo theme_option('facebook'); ?></p>
                    <p class="is-size-5 mb-2"><?php echo theme_option('instagram'); ?></p>
                    <p class="is-size-5 mb-2"><?php echo theme_option('youtube'); ?></p>
                    <p class="is-size-5 mb-2"><?php echo theme_option('sdt'); ?></p>
                </div>
            </div>
        </div>
    </section>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
    AOS.init();
    </script>
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
