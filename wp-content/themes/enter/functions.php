<?php
add_action('init', 'myStartSession', 1);
function myStartSession() {
    if(!session_id()) {
        session_start();
    }
}
if ( ! function_exists( 'enter_setup' ) ) :
function enter_setup() {
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'thumbnails' );
	set_post_thumbnail_size( 320, 320, true );
	add_image_size("thumb-300", 300, 300, true );
	add_image_size("thumb-250", 250, 150, true );
	register_nav_menus( array(
		'menu_top' => __( 'Menu Chính', 'enter' ),
		'menu_1'  => __( 'Menu Giới thiệu', 'enter' ),
		'menu_2'  => __( 'Menu Trợ giúp', 'enter' ),
		'menu_3'  => __( 'Menu Footer', 'enter' ),
	) );
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // enter_setup
function getchars($str, $sl){
	$str = strip_tags($str);
	$mang = explode(" ",$str);
	$chuoi = "";
	
	for($i=0;$i<$sl && $i < strlen($str);$i++){
		$chuoi .= $mang[$i]." ";
	}
	if(strlen($str) == strlen(trim($chuoi)))
		return trim($chuoi);
	return trim($chuoi)."...";
}
function getchar($str, $sl){
	$str = str_replace("  "," ",strip_tags($str));
	$chuoi = substr($str,0,$sl);
	if(strlen($str) == strlen(trim($chuoi)))
		return trim($chuoi);
	return trim($chuoi)."...";
}
function value($s,$from,$to){
	$s = explode($from,$s);
	$s = explode($to,$s[1]);
	return $s[0];
}
function get_list_img($post_id){
	$post_content = get_post($post_id);
	$s = $post_content->post_content;
	$s = strip_tags($s,"<img>");
	$arr = explode("<img",$s);
	$abr = array();
	for($i=1;$i<count($arr);$i++){
		$temp = value($arr[$i],'src="','"');
		if(strlen($temp) > 10) $abr[] = $temp;
	}
	return $abr;
}
function gets_thumbnail( $size = 'thumbnails', $id = NULL, $attr = array() ) {
	global $post, $ar2_image_sizes;	
	//if ( $post ) $id = $post->ID;	
	
	if ( !key_exists( 'alt', $attr ) )
		$attr['alt'] = esc_attr( get_the_excerpt() );
	if ( !key_exists( 'title', $attr ) )
		$attr['title'] = esc_attr( get_the_title() );
	if ( has_post_thumbnail( $id ) ) {
		$link = getimg(get_the_post_thumbnail( $id, $size, $attr ));
	}else{
		$posttemp = get_post($id);
		$link = getimg($posttemp->post_content);
	}
	return '<img src="' . $link . '" />'; 
}
function gets_thumbnails($id = NULL, $attr = array() ) {
	if($id == NULL) return getimgs("");
	$size = 'thumbnails';
	if ( !key_exists( 'alt', $attr ) )
		$attr['alt'] = esc_attr( get_the_excerpt() );
	if ( !key_exists( 'title', $attr ) )
		$attr['title'] = esc_attr( get_the_title() );
	if ( has_post_thumbnail( $id ) ) {
		$link = getimgs(get_the_post_thumbnail( $id, $size, $attr ));
	}else{
		$posttemp = get_post($id);
		$link = getimgs($posttemp->post_content);
	}
	return $link; 
}
function getimgs($str){
	$arr_temp = explode("<img",$str);
	if(count($arr_temp) < 2) return get_template_directory_uri()."/images/noimgs.png";
	//$i = rand(1,count($arr_temp)-1);
	$i = 1;
	$arr_temp = explode("src=",$arr_temp[$i]);
	$arr_temp = str_replace("'","",$arr_temp[1]);
	$arr_temp = str_replace('"',"",$arr_temp);
	$arr_temp = explode(" ",$arr_temp);
	if($arr_temp[0])
		return $arr_temp[0];
	return get_template_directory_uri()."/images/noimgs.png";
}
function getimg($str){
	$arr_temp = explode("<img",$str);
	if(count($arr_temp) < 2) return get_template_directory_uri()."/images/noimg.jpg";
	//$i = rand(1,count($arr_temp)-1);
	$i = 1;
	$arr_temp = explode("src=",$arr_temp[$i]);
	$arr_temp = str_replace("'","",$arr_temp[1]);
	$arr_temp = str_replace('"',"",$arr_temp);
	$arr_temp = explode(" ",$arr_temp);
	if($arr_temp[0])
		return $arr_temp[0];
	return get_template_directory_uri()."/images/noimg.jpg";
}
add_action( 'after_setup_theme', 'enter_setup' );
function enter_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'enter' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'enter' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title"><span>',
		'after_title'   => '</span></h4>',
	) );
}
add_action( 'widgets_init', 'enter_widgets_init' );
function enter_scripts() {
	wp_enqueue_style( 'enter-style', get_stylesheet_uri() );

}
add_action( 'wp_enqueue_scripts', 'enter_scripts' );

function enter_body_classes( $classes ) {
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'enter_body_classes' );
require get_template_directory() . '/inc/theme-option.php';
require get_template_directory() . '/inc/MostPost.php';
require get_template_directory() . '/inc/MNPost.php';
require get_template_directory() . '/inc/shortcode.php';

function enter_posted_on() {

	// Get the author name; wrap it in a link.
	$byline = '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_the_author() . '</a></span>';

	// Finally, let's write all of this to the page.
	echo '<span class="byline">Bởi ' . $byline . '</span> - <span class="posted-on">' . get_the_date() . '</span>';
}
function bai_viet_chuyen_muc( $atts ) {
	extract(shortcode_atts(array('limit' => '10',), $atts));
	global $post;
	$categories = get_the_category($post->ID);
	if ($categories) {
		$category_ids = array();
		foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
		$args=array(
			'category__in' => $category_ids,
			'post__not_in' => array($post->ID),
			'showposts'=>$limit,
			'caller_get_posts'=>1,
			'orderby'=>'rand'
		);
		$related_cat = '<div class="widget_bvlq widget"><h4 class="widget-title"><span>Bài viết liên quan</span></h4><ul>';
		$rl_cat = new wp_query($args);
		if( $rl_cat->have_posts() ) {
			while ($rl_cat->have_posts()) {
				$rl_cat->the_post();
				$post = get_post($post_id);
				$title=$post->post_title;
				$link=get_permalink($post->id);
				$img = gets_thumbnail("thumb-320");				
				$related_cat.= '<li><a href="'.$link.'" title="'.$title.'" rel="bookmark"><div class="img">'.$img.'</div><h4 class="entry-title">'.$title.'</h4></a></li>';
			}
		}
		$related_cat .="</ul><div class='clear' ></div></div>";
		wp_reset_query();
	}
	return $related_cat;
}
add_shortcode('baivietcm','bai_viet_chuyen_muc');
function postview_set($postID) {
    $count_key = 'postview_number';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

if ( ! function_exists( 'login_menu' ) ) :
function login_menu() {
?>
<ul class="bar-login">
						<li class="login"><a href="#">Đăng nhập</a></li>
						<li class="register"><a href="#">Đăng ký</a></li>
					</ul>
<?php
}
endif; // login_menu

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
#acf-color, #acf-color_code, #acf-gia{float: left; width: 33%; box-sizing: border-box !important;}
.acf_postbox:after{content: " "; display: block; clear: both;}
  </style>';
}

function get_first_color($id){
	$color = explode("\r\n",get_field("color_code"));
	return $color[0];
}
function tag_info($id){
	$stt = intval(get_field("status"));
	$arr = array(
		10 => "Không",
		0 => "New",
		1 => "Sale",
		2 => "Trả góp",
		3 => "Trả góp 0 %",
		4 => "Mua nhiều",
		5 => "Hàng công ty + 1 đổi 1",
		6 => "1 Đổi 1 trong 30 ngày",
	);
	if($stt == 10) return "";
	return "<span class='tag_info taginfo$stt'>".$arr[$stt]."</span>";
}
function remove_menus(){
  
  remove_menu_page( 'index.php' );                  //Dashboard
  remove_menu_page( 'jetpack' );                    //Jetpack* 
  //remove_menu_page( 'edit.php' );                   //Posts
  remove_menu_page( 'upload.php' );                 //Media
  //remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  //remove_menu_page( 'themes.php' );                 //Appearance
  //remove_menu_page( 'plugins.php' );                //Plugins
  remove_menu_page( 'users.php' );                  //Users
  remove_menu_page( 'tools.php' );                  //Tools
  remove_menu_page( 'options-general.php' );        //Settings
  remove_menu_page( 'wpseo_dashboard' );
  remove_menu_page( 'edit.php?post_type=acf' );
  remove_menu_page( 'all-in-one-seo-pack' );
  //remove_menu_page( 'wpcf7' );
  
}
add_filter( 'aioseo_custom_menu_order', '__return_false' );
//add_action( 'admin_menu', 'remove_menus' );
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
function get_info(){
	$s = str_replace("</tr><br />","</tr>",html_entity_decode(get_field("info")));
	$s = str_replace("<tr><br />","<tr>",$s);
	$s = str_replace("<td><br />","<td>",$s);
	$s = str_replace("</td><br />","</td>",$s);
	$s = str_replace("<tbody><br />","<tbody>",$s);
	$s = str_replace('width="100%"><br />','width="100%">',$s);

	return $s;
}

function order_category_archives( $query ) {
	if ( is_category() && $query->is_main_query() && isset($_REQUEST['filter'])){		
		$order = intval($_REQUEST['filter']);
		if($order < 0 || $order > 3) $order = 0;
		if($order == 1){
			$query->set( 'order', 'ASC' );
		}
		elseif($order == 2){
			$query->set( 'orderby','meta_value_num' );
			$query->set( 'meta_key','gia_hien_thi' );
			$query->set( 'order','DESC' );
		}
		elseif($order == 3){
			$query->set( 'meta_key', 'gia_hien_thi' );
			$query->set( 'orderby', 'meta_value_num' );
			$query->set( 'order', 'ASC' );
		}else{$query->set( 'order', 'DESC' );}
	}
	if(is_search() && $query->is_main_query()){		
		if(isset($_REQUEST['filter'])){
			$order = intval($_REQUEST['filter']);
			if($order < 0 || $order > 3) $order = 0;
			if($order == 1){
				$query->set( 'order', 'ASC' );
			}
			elseif($order == 2){
				$query->set( 'orderby','meta_value_num' );
				$query->set( 'meta_key','gia_hien_thi' );
				$query->set( 'order','DESC' );
			}
			elseif($order == 3){
				$query->set( 'meta_key', 'gia_hien_thi' );
				$query->set( 'orderby', 'meta_value_num' );
				$query->set( 'order', 'ASC' );
			}else{$query->set( 'order', 'DESC' );}
		}
		$query->set( 'cat', '-1' );
		$query->set( 'post_type', 'post' );
	}
}

add_action( 'pre_get_posts', 'order_category_archives' );



function getParentCategory($id){
	if($id == 0 || $id == "") return "";
	$str = "";
	$categories = get_categories(array('parent' => $id));
	foreach ( $categories as $cat ) {
		$str .= ",".$cat->term_id;
	}
	$current = get_category($id);
	if($current->parent != 0 && $current->parent != ""){
		$str .= getParentCategory($current->parent);
	}
	return $str;
}

function splienquan_func( $atts ) {
	extract(shortcode_atts(array('limit' => '4',), $atts));
	global $post;
	$categories = get_the_category($post->ID);
	if ($categories) {
		$category_ids = array();
		foreach($categories as $individual_category){
			$category_ids[] = $individual_category->term_id;
			if($individual_category->parent != 0 && $individual_category->parent != ""){
				$list = explode(",",getParentCategory($individual_category->parent));
				foreach($list as $id){
					if($id != "" && $id != 0){
						$category_ids[] = $id;
					}
				}
			}
		}
		$args=array(
			'category__in' => $category_ids,
			'post__not_in' => array($post->ID),
			'showposts'=>$limit,
			'ignore_sticky_posts'=>1,
			'orderby'=>'rand'
		);
		$related_cat = '<div class="samepro"><h4 class="widget-title"><span>Sản phẩm liên quan</span></h4><ul>';
		$rl_cat = new wp_query($args);
		if( $rl_cat->have_posts() ) {
			while ($rl_cat->have_posts()) {
				$rl_cat->the_post();
				$title=getchar($post->post_title,27);
				$link=get_permalink($post->id);
				$img = gets_thumbnail("thumb-300");
				$related_cat.= '<li><a href="'.$link.'" title="'.$post->post_title.'">'.$img.'<h5 class="naviacc-name">'.$title.'</h5><p class="price">'.format_tien(get_field("gia_hien_thi",$post->id)).'₫</p></a></li>';
			}
		}
		$related_cat .="</ul><div class='clear' ></div></div>";
		wp_reset_query();
	}
	return $related_cat;
}
add_shortcode('splienquan','splienquan_func');

add_action( 'wp_enqueue_scripts', 'wcqib_enqueue_polyfill' );
function wcqib_enqueue_polyfill() {
    wp_enqueue_script( 'wcqib-number-polyfill' );
}

function your_function() {
    if( function_exists('WC') ){
        WC()->cart->empty_cart();
    }
}
add_action('wp_logout', 'your_function');

add_filter('woocommerce_default_address_fields', 'override_default_address_checkout_fields', 20, 1);
function override_default_address_checkout_fields( $address_fields ) {
    $address_fields['first_name']['placeholder'] = 'First Name';
    $address_fields['last_name']['placeholder'] = 'Last Name';
    $address_fields['address_1']['placeholder'] = 'Adresse';
    $address_fields['city']['placeholder'] = 'City';
    $address_fields['phone']['placeholder'] = 'Phone number';
    return $address_fields;
}

add_filter( 'woocommerce_checkout_fields' , 'override_billing_checkout_fields', 20, 1 );
function override_billing_checkout_fields( $fields ) {
    $fields['billing']['billing_email']['placeholder'] = 'Email';
    $fields['billing']['billing_phone']['placeholder'] = 'Phone';
    return $fields;
}

function pagination_tdc() {
	if( is_singular() )
	return;
	global $wp_query;
	/** Ngừng thực thi nếu có ít hơn hoặc chỉ có 1 bài viết */
	if( $wp_query->max_num_pages <= 1 )
	return;
	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max = intval( $wp_query->max_num_pages );
 
	/** Thêm page đang được lựa chọn vào mảng*/
	if ( $paged >= 1 )
	$links[] = $paged;
	/** Thêm những trang khác xung quanh page được chọn vào mảng */
	if ( $paged >= 3 ) {
		   $links[] = $paged - 1;
		   $links[] = $paged - 2;
	 }
 
	 if ( ( $paged + 2 ) <= $max ) {
		   $links[] = $paged + 2;
		   $links[] = $paged + 1;
	  }
 
 /** Hiển thị thẻ đầu tiên \n để xuống dòng code */
  echo '<ul class="pagination">' . "\n";
 
  /** Hiển thị link về trang trước */
  if ( get_previous_posts_link() )
  printf( '<li>%s</li>' . "\n", get_previous_posts_link('Trước') );
 
  /** Nếu đang ở trang 1 thì nó sẽ hiển thị đoạn này */
  if ( ! in_array( 1, $links ) ) {
  $class = 1 == $paged ? ' class="active"' : '';
  printf( '<li %s><a rel="nofollow" class="page larger" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
  if ( ! in_array( 2, $links ) )
  echo '<li>…</li>';
  }
 
  /** Hiển thị khi đang ở một trang nào đó đang được lựa chọn */
  sort( $links );
  foreach ( (array) $links as $link ) {
  $class = $paged == $link ? ' class="active"' : '';
  printf( '<li%s><a rel="nofollow" class="page larger" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
  }
 
  /** Hiển thị khi đang ở trang cuối cùng */
  if ( ! in_array( $max, $links ) ) {
  if ( ! in_array( $max - 1, $links ) )
  echo '<li>…</li>' . "\n";
  $class = $paged == $max ? ' class="active"' : '';
  printf( '<li%s><a rel="nofollow" class="page larger" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
  }
 
  /** Hiển thị link về trang sau */
  if ( get_next_posts_link() )
  printf( '<li>%s</li>' . "\n", get_next_posts_link('Sau') );
  echo '</ul>' . "\n";
 }
 @ini_set( 'upload_max_size' , '200M' );
 @ini_set( 'post_max_size', '200M');
 @ini_set( 'max_execution_time', '300' );
 add_filter('woocommerce_catalog_orderby', 'wc_customize_product_sorting');
      // fix polylang language segmentation
      add_action( 'rest_api_init' , array( $this, 'polylang_json_api_init') );
      add_action( 'rest_api_init' , array( $this, 'polylangroute' ) );

function polylang_json_api_init(){
      global $polylang;
      $default = pll_default_language();
      $langs = pll_languages_list();
      $cur_lang = $_GET['lang'];
      if (!in_array($cur_lang, $langs)) {
          $cur_lang = $default;
      }
      $polylang->curlang = $polylang->model->get_language($cur_lang);
      $GLOBALS['text_direction'] = $polylang->curlang->is_rtl ? 'rtl' : 'ltr';
  }
function polylang_json_api_languages(){
      return pll_languages_list();
  }