(function($) {
    $(function() {
        // index 0
        var jcarousel0 = $('.jcarousel-0');

        jcarousel0
            .on('jcarousel:reload jcarousel:create', function() {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 480) {
                    width = width / 2;
                }

                carousel
                    .jcarousel('items')
                    .css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev-0').jcarouselControl({
            target: '-=1'
        });

        $('.jcarousel-control-next-0').jcarouselControl({
            target: '+=1'
        });

        // index 1
        var jcarousel0 = $('.jcarousel-1');

        jcarousel0
            .on('jcarousel:reload jcarousel:create', function() {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 480) {
                    width = width / 2;
                }

                carousel
                    .jcarousel('items')
                    .css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev-1').jcarouselControl({
            target: '-=1'
        });

        $('.jcarousel-control-next-1').jcarouselControl({
            target: '+=1'
        });
    });
})(jQuery);
