<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <section id="who-we-are" class="pb-8">
        <div class="container"><?php $post = get_post(15)?>
            <div class="columns mb-8" data-aos="fade-up" data-aos-duration="1000" data-aos-offset="100">
                <div class="column is-5 is-relative">
                    <figure class="image">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>" />
                    </figure>
                </div>
                <div class="column is-7 px-7 py-8">
                    <div class="title">
                        <h2 class="has-text-weight-bold"><?php echo $post->post_title; ?></h2>
                    </div>
                    <div class="level">
                        <div class="level-left">
                            <span class="slash is-italic">/</span>
                        </div>
                        <div class="level-right">
                            <p class="question has-text-right"><u class="is-size-4">Tại sao lại là enter?</u></p>
                        </div>
                    </div>
                    <div class="is-size-5 has-text-justified"><?php echo apply_filters('the_content', $post->post_content); ?></div>
                </div>
            </div>
            <div class="columns" data-aos="fade-up" data-aos-duration="1000" data-aos-offset="100">
                <div class="column pl-7"><?php $post = get_post(17)?>
                    <div class="is-size-5 has-text-justified"><?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                </div>
                <div class="column is-relative">
                    <figure class="image">
                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>" />
                    </figure>
                </div>
            </div>
        </div>
    </section>
    <section id="quote" class="my-7 py-6">
    <?php $post = get_post(19)?>
        <div class="container is-relative">
            <div class="quote quote-start">&ldquo;</div>
            <p class="p-4"><?php echo apply_filters('the_content', $post->post_content); ?></p>
            <div class="quote quote-end">&rdquo;</div>
        </div>
    </section>
    <section id="our-clients" class="py-8">
        <div class="container">
            <div class="columns mb-7">
                <div class="column"></div>
                <div class="column">
                    <div class="title">
                        <h2 class="has-text-weight-bold">Our clients</h2>
                    </div>
                    <div class="level">
                        <div class="level-left">
                            <span class="slash is-italic">/</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns images-our is-multiline is-vcentered">
                <div class="column image-our is-2 mb-7">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partner-1.png">
                    </figure>
                </div>
                <div class="column image-our is-2 mb-7">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partner-2.png">
                    </figure>
                </div>
                <div class="column image-our is-2 mb-7">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partner-3.png">
                    </figure>
                </div>
                <div class="column image-our is-2 mb-7">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partner-4.png">
                    </figure>
                </div>
                <div class="column image-our is-2 mb-7">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partner-5.png">
                    </figure>
                </div>
                <div class="column image-our is-2 mb-7">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partner-6.png">
                    </figure>
                </div>
                <div class="column image-our is-2 mb-7">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partner-7.png">
                    </figure>
                </div>
                <div class="column image-our is-2 mb-7">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partner-8.png">
                    </figure>
                </div>
                <div class="column image-our is-2 mb-7">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partner-9.png">
                    </figure>
                </div>
                <div class="column image-our is-2 mb-7">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partner-10.png">
                    </figure>
                </div>
                <div class="column image-our is-2 mb-7">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partner-11.png">
                    </figure>
                </div>
                <div class="column image-our is-2 mb-7">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partner-12.png">
                    </figure>
                </div>
            </div>
        </div>
    </section>
    <section id="what-we-do" class="py-8">
        <div class="container">
            <div class="columns mb-7">
                <div class="column"></div>
                <div class="column">
                    <div class="title">
                        <h2 class="has-text-weight-bold">What we do</h2>
                    </div>
                    <div class="level">
                        <div class="level-left">
                            <span class="slash is-italic">/</span>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                $args = array( 'category' => 3, 'post_type' =>  'post' ); 
                $postslist = get_posts( $args );
                foreach ($postslist as $post) :
            ?>  
            <div class="jcarousel-wrapper project-item mb-5">
                <div class="project-title project-title-<?php echo $post->ID ?> is-size-4">
                </div>
                <div class="project-title-mobile mb-3">
                    <div class="container">
                        <h3 class="is-size-4"><?php echo $post->post_title; ?></h3>
                    </div>
                </div>
                <div class="jcarousel jcarousel-0">
                    <ul>
                    <?php 
                        $values = get_field('image', $post->ID );
                        $count = count($values);
                        for ($i=0; $i <= 5; $i++) { 
                    ?>
                        <li class="mr-3">
                            <a href="javascript:void(0);" class="open-modal" data-project-id="<?php echo $post->ID ?>">
                                <figure class="image">
                                    <img src="<?php echo $values[$i]["url"] ?>">
                                    <span class="item-name is-size-4 is-uppercase">Shogun</span>
                                </figure>
                            </a>
                        </li>
                        <li class="mr-3">
                            <div class="columns is-multiline is-mobile is-variable is-1">
                                <div class="column is-6 py-3">
                                    <a href="javascript:void(0);" class="open-modal" data-project-id="<?php echo $post->ID ?>">
                                        <figure class="image">
                                            <img src="<?php echo $values[$i+1]['url']?>">
                                            <span class="item-name item-small-name is-size-4 is-uppercase">Shogun</span>
                                        </figure>
                                    </a>
                                </div>
                                <div class="column is-6 py-3">
                                    <a href="javascript:void(0);" class="open-modal" data-project-id="<?php echo $post->ID ?>">
                                        <figure class="image">
                                            <img src="<?php echo $values[$i+2]['url']?>">
                                            <span class="item-name item-small-name is-size-4 is-uppercase">Shogun</span>
                                        </figure>
                                    </a>
                                </div>
                                <div class="column is-6 py-3">
                                    <a href="javascript:void(0);" class="open-modal" data-project-id="<?php echo $post->ID ?>">
                                        <figure class="image">
                                            <img src="<?php echo $values[$i+3]['url']?>">
                                            <span class="item-name item-small-name is-size-4 is-uppercase">Shogun</span>
                                        </figure>
                                    </a>
                                </div>
                                <div class="column is-6 py-3">
                                    <a href="javascript:void(0);" class="open-modal" data-project-id="<?php echo $post->ID ?>">
                                        <figure class="image">
                                            <img src="<?php echo $values[$i+4]['url']?>">
                                            <span class="item-name item-small-name is-size-4 is-uppercase">Shogun</span>
                                        </figure>
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <a href="#" class="jcarousel-control-prev jcarousel-control-prev-0">&lsaquo;</a>
                <a href="#" class="jcarousel-control-next jcarousel-control-next-0">&rsaquo;</a>
                <p class="jcarousel-pagination"></p>
            </div>
            <?php break;}?>
            <?php endforeach; ?>
            <div class="modal is-clipped" id="modal-box">
                <div class="modal-background"></div>
                <div class="modal-content has-background-white p-4">
                    <p class="mb-4 is-size-4 modal-project-title"></p>
                    <div class="modal-project-content">
                    </div>
                </div>
                <button class="modal-close is-large" aria-label="close" id="modal-close"></button>
            </div>
        </div>
    </section>
		</main><!-- .site-main -->
    </div><!-- .content-area -->
    
<?php get_footer(); ?>

<script>
    let parthName = ''
    if(window.location.pathname === '/en/'){
        parthName = 'en'
    }else{
        parthName = 'vi'
    }
const api = 'http://enter.dev/api/';
const slug = (window.location.pathname === '/en/')?'project-en':'project'
$.ajax({
    url : api + 'get_category_posts/', // gửi ajax đến file result.php
    type : "get", // chọn phương thức gửi là get
    dateType:"text", // dữ liệu trả về dạng text
    data : { // Danh sách các thuộc tính sẽ gửi đi
        slug :  'project'
    },
    success : function (result){
        console.log(result)
        var projects = []
        for (let i = 0; i < result.posts.length; i++) {
            const Obj = new Object;
            const images = new Array;
            Obj['title'] = result.posts[i].title;
            Obj['id'] = result.posts[i].id;
            for (let j = 0; j < result.posts[i].attachments.length; j++) {
                if(result.posts[i].attachments[j].parent === result.posts[i].attachments[j].parent){
                    const img = new Object;
                    img['url'] = result.posts[i].attachments[j].url;
                    img['title'] = '';
                    images.push(img);
                }
            }
            Obj['images'] = images;
            projects.push(Obj)
        }
        for (var i = 0; i < projects.length; i++) {
            var words = projects[i].title.split(' ');
            for (var j = 0; j < words.length; j++) {
                var str_word = ''
                for (var characterIndex = 0; characterIndex < words[j].length; characterIndex++) {
                    str_word += '<div class="character">' + words[j][characterIndex] + '</div>';
                }
                $('.project-title-' + projects[i].id).append('<div class="word has-text-centered ml-6">' + str_word + '</div>');
            }
        }
        $('.open-modal').click(function() {
            var index = $(this).data('projectId');
            const result = projects.filter(project => project.id === index);
            $('#modal-box .modal-project-title').text(result[0].title);
            for (var i = 0; i < result[0].images.length; i++) {
                $('#modal-box .modal-project-content').append('<figure class="image"><img src = "' + result[0].images[i].url + '" ></figure>')
            }
            $('#modal-box').addClass('is-active');
        });
        $('#modal-close').click(function() {
            $('#modal-box').removeClass('is-active');
            $( "#modal-box .modal-content .modal-project-content" ).empty();
        });
    }
});
            $(document).ready(function() {
                // Check for click events on the navbar burger icon
                $('.navbar-burger').click(function() {
                    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                    $('.navbar-burger').toggleClass('is-active');
                    $('.navbar-menu').toggleClass('is-active');
                });
            });
</script>
