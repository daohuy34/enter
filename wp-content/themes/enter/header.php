<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
	<?php endif; ?>
    <?php wp_head(); ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.2.0/aos.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.2.0/aos.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jcarousel.responsive.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/spacing.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jcarousel.responsive.js"></script>
</head>

<body>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<div class="site-inner">
    <section id="header" class="pt-3 pb-4">
        <div class="container">
            <nav class="navbar" role="navigation" aria-label="main navigation">
                <div class="navbar-brand">
                    <a class="navbar-item" href="https://bulma.io">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="">
                    </a>
                    <a
                            role="button"
                            class="navbar-burger burger"
                            aria-label="menu"
                            aria-expanded="false"
                            data-target="navbarBasicExample"
                        >
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                        </a>
                </div>
                <?php
                    if ( has_nav_menu( 'menu_top' ) ){  wp_nav_menu( array('theme_location' => 'menu_top','menu_class'=> 'primary-menu') ); }
                ?>
                <div id="weglot_here"></div>
            </nav>
        </div>
    </section>
    <section id="slideshow">
        <video autoplay muted loop id="myVideo">
            <source src="<?php echo get_template_directory_uri(); ?>/static/showreel.mp4" type="video/mp4">
            Your browser does not support HTML5 video.
        </video>
    </section>
		<div id="content" class="site-content">
        <script>
            $(document).ready(function() {
                // Check for click events on the navbar burger icon
                $('.navbar-burger').click(function() {
                    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                    $('.navbar-burger').toggleClass('is-active');
                    $('.menu-menu-chinh-container').toggleClass('is-active');
                    $('.country-selector').toggleClass('is-active');
                });
            });
        </script>