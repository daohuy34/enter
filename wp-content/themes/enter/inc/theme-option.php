<?php
add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' ); 

function theme_options_init(){
 register_setting( 'sample_options', 'sample_theme_options');
} 
function theme_option($field){
	$options = get_option( 'sample_theme_options' );
	return $options[$field];
}
function theme_options_add_page() {
 add_theme_page( __( 'Theme Options', 'sampletheme' ), __( 'Theme Options', 'sampletheme' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}
function theme_options_do_page() {
	global $select_options;
	if ( ! isset( $_REQUEST['settings-updated'] ) ) $_REQUEST['settings-updated'] = false; 
?>
<div class="wrap">
<?php screen_icon(); ?><h2>Theme Option</h2>
<form method="post" action="options.php">
<?php settings_fields( 'sample_options' ); $options = get_option( 'sample_theme_options' ); ?> 
	<table class="form-table">
		<tr valign="top">
			<th width="90px">Tên Website</th>
			<td><input type="text" name="sample_theme_options[website]" id="sample_theme_options[website]" style="width: 85%" value="<?php echo $options['website']; ?>" ></td>
        </tr>
        <tr valign="top">
			<th width="90px">SDT</th>
			<td><input type="text" name="sample_theme_options[sdt]" id="sample_theme_options[sdt]" style="width: 85%" value="<?php echo $options['sdt']; ?>" ></td>
        </tr>
        <tr valign="top">
			<th width="90px">Địa chỉ</th>
			<td><input type="text" name="sample_theme_options[address]" id="sample_theme_options[address]" style="width: 85%" value="<?php echo $options['address']; ?>" ></td>
        </tr>
        <tr valign="top">
			<th width="90px">Facebook</th>
			<td><input type="text" name="sample_theme_options[facebook]" id="sample_theme_options[facebook]" style="width: 85%" value="<?php echo $options['facebook']; ?>" ></td>
        </tr>
        <tr valign="top">
			<th width="90px">instagram</th>
			<td><input type="text" name="sample_theme_options[instagram]" id="sample_theme_options[instagram]" style="width: 85%" value="<?php echo $options['instagram']; ?>" ></td>
        </tr>
        <tr valign="top">
			<th width="90px">youtube</th>
			<td><input type="text" name="sample_theme_options[youtube]" id="sample_theme_options[youtube]" style="width: 85%" value="<?php echo $options['youtube']; ?>" ></td>
        </tr>
        <tr valign="top">
			<th width="90px">vimeo</th>
			<td><input type="text" name="sample_theme_options[youtube]" id="sample_theme_options[vimeo]" style="width: 85%" value="<?php echo $options['vimeo']; ?>" ></td>
        </tr>
    </table>    
    <?php submit_button(); ?>
</form></div>
<?php } ?>